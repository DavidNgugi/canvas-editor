import { checkIfMouseOverObject } from './utils/Collider';
import { _e } from './utils/GeneralUtils';
import { inRange } from './utils/MathUtil';

export default class EventListener {
    
    /**
     * Set initial properties
     * @param {object} editor 
     */
    constructor(editor){
        this.editor = editor;
        this.keysDown = [];
        this.mouseDown = false;
        this.dragging = false;
        this.radius = 20; // radius of influence for mouse dragging
        this.mousePosition = {};
        this.offset = {};
        this.offsetX = _e('canvas').getBoundingClientRect().left;
        this.offsetY = _e('canvas').getBoundingClientRect().top;
        this.target = null;
        this.startX = 0;
        this.startY = 0;
    }

    /**
     * Listen to a given event
     * @param {string} event 
     */
    listenTo(event){
        switch(event){
            case 'mousedown':
                _e('canvas').addEventListener(event, (e) => { this.mouseDownHandler(e) });
                break;
            case 'mousemove':
                _e('canvas').addEventListener(event, (e) => { this.mouseMoveHandler(e) });
                break;
            case 'mouseup':
                _e('canvas').addEventListener(event, (e) => { this.mouseUpHandler(e) });
                break;
            case 'keydown':
                _e('canvas').addEventListener(event, (e) => { this.keyDownHandler(e) });
                break;
            case 'keyup':
                _e('canvas').addEventListener(event, (e) => { this.keyDownHandler(e) });
                break;
        }
    }

    /**
     * Listen to all mouse events
     */
    listenToAllEvents(){
        _e('canvas').addEventListener('mousedown', (e) => { this.mouseDownHandler(e) });
        _e('canvas').addEventListener('mousemove', (e) => { this.mouseMoveHandler(e) });
        _e('canvas').addEventListener('mouseup', (e) => { this.mouseUpHandler(e) });
        _e('canvas').addEventListener('keydown', (e) => { this.keyDownHandler(e) });
        _e('canvas').addEventListener('keyup', (e) => { this.keyDownHandler(e) });
    }

    /**
     * Handler for keydown events
     * @param {object} e - Event
     */
    keyDownHandler(e){
        e.preventDefault();

        // check for combinations

    }

    /**
     * Handler for mouseup events
     * @param {object} e - Event
     */
    keyUpHandler(e){
        e.preventDefault();


    }

    /**
     * Handler for mousedown events
     * @param {object} e - Event
     */
    mouseDownHandler(e){
        e.preventDefault();
        this.mouseDown = true;
        this.mousePosition.x = e.clientX - this.offsetX;
        this.mousePosition.y = e.clientY - this.offsetY;

        // if colliding with an object, check for move event
        this.editor.components.forEach((target) => {

            if(checkIfMouseOverObject(this.mousePosition, target.component)){
                // Mouse found target
                this.target = target;

                console.log('Mouse over ' + this.target.component.name +' #'+this.target.id);

                this.dragging = true;

                this.target.isDraggging = true;
                
            }

            this.startX = this.mousePosition.x;
            this.startY = this.mousePosition.y;
        });
    }

    /**
     * Handler for mousemove events
     * @param {object} e - Event
     */
    mouseMoveHandler(e){
        e.preventDefault();

        if(this.mouseDown && this.target != null && this.dragging){
            // Get end mouse position
            var endX = e.clientX - this.offsetX,
                endY = e.clientY - this.offsetY;

            var dX = endX - this.startX,
                dY = endY - this.startY;

            var targetGoalX = this.target.component.x + dX;
            var targetGoalY = this.target.component.y + dY;

            this.target.component.borderColor = "red";

            if(inRange(targetGoalX, 10, _e('canvas').width - this.target.component.width) && inRange(targetGoalY, 20, _e('canvas').height - this.target.component.height)){
                this.target.component.x += dX;
                this.target.component.y += dY;

                this.editor.update(this.target);

                this.editor.render();
            }
            // Reset
            this.startX = endX;
            this.startY = endY;
        }
    }

    /**
     * Handler for mouseup events
     * @param {object} e - Event
     */
    mouseUpHandler(e){
        e.preventDefault();

        if(this.target != null && this.dragging){
            console.log('Target let go at: '+ this.target.component.x+','+ this.target.component.y);

            this.mouseDown = false;

            this.mousePosition = {};

            this.offset = {};

            this.target.component.borderColor = "lightblue";

            this.target.component.isDraggging = false;

            this.editor.update(this.target);

            this.target = null;

            console.log('Mouse let go => { x: '+ e.clientX + ', y: '+ e.clientY +'}');
        }

        // Clear event listeners
        _e('canvas').removeEventListener('mousedown', (e) => { this.mouseDownHandler(e); } );
        _e('canvas').removeEventListener('mouseup', (e) => { this.mouseUpHandler(e); } );
        _e('canvas').removeEventListener('mousemove', (e) => { this.mouseMoveHandler(e); } );
        
    }

    
}