'use strict';

import * as Math from './MathUtil';

export function checkIfMouseOverObject(position, target){
    
    if(Math.pointInRect(position.x, position.y, target)){
        return true;
    }

    return false;
}