/**
 * Calculates the height of a given font using a neat little trick
 * Special thanks to Daniel Earwicker <https://github.com/danielearwicker/carota>
 * @param {string} font 
 */
export function getTextHeight(font) {
    var text = $('<span>Hg</span>').css({ fontFamily: font });
    var block = $('<div style="display: inline-block; width: 1px; height: 0px;"></div>');

    var div = $('<div></div>');
    div.append(text, block);

    var body = $('body');
    body.append(div);

    try {

        var result = {};

        block.css({ verticalAlign: 'baseline' });
        result.ascent = block.offset().top - text.offset().top;

        block.css({ verticalAlign: 'bottom' });
        result.height = block.offset().top - text.offset().top;

        result.descent = result.height - result.ascent;

    } finally {
        div.remove();
    }

    return result;
}

/**
 * Get and element by ID, Similar to jQuery's $()
 * @param {string} id 
 */
export function _e(id) { return document.getElementById(id); }

/**
 * Get and element by Class Name, Similar to jQuery's $()
 * @param {string} id 
 */
export function _c(className) { return document.getElementsByClassName(className); }


export function resizeCanvasToDisplaySize(canvas) {
   // look up the size the canvas is being displayed
   const width = canvas.clientWidth;
   const height = canvas.clientHeight;

   // If it's resolution does not match change it
   if (canvas.width !== width || canvas.height !== height) {
        canvas.width = width;
        canvas.height = height;
        return true;
    }
   return false;
}