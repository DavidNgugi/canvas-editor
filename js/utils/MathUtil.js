'use strict';

/**
 * Linear numbererpolation
 * @param {number} current
 * @param {number} goal
 * @param {number} dt
 * @return {number}
 */
export function lerp(current, goal, dt){
    return current + (goal-current) * dt;
}

/**
 * Converts a Degree value to Radians
 * @param {number} value - Value in Degrees
 * @return {number}
*/
export function toRadians(value){
   return (value * Math.PI) / 180;
}

/**
 * Converts a value in Radians to Degrees
 * @param {number} value
 * @return {number}
*/
export function toDegrees(value){
   return (value * 180) / Math.PI;
}

/**
 * Clamps/restricts a value to a given range
 * @param {number} value 
 * @param {number} min 
 * @param {number} max 
 * @return {number}
 */
export function clamp(value, min, max){
    return value > max ? max : (value < min ? min : value);
}


export function norm(value, min, max) {
    return (value - min) / (max - min);
}

export function distance(p0, p1) {
    var dx = p1.x - p0.x,
        dy = p1.y - p0.y;
    return Math.sqrt(dx * dx + dy * dy);
}

export function distanceXY(x0, y0, x1, y1) {
    var dx = x1 - x0,
        dy = y1 - y0;
    return Math.sqrt(dx * dx + dy * dy);
}

export function circleCollision(c0, c1) {
    return distance(c0, c1) <= c0.radius + c1.radius;
}

export function circlePointCollision(x, y, circle) {
    return distanceXY(x, y, circle.x, circle.y) < circle.radius;
}

export function pointInRect(x, y, rect) {
    return inRange(x, rect.x, rect.x + rect.width) &&
           inRange(y, rect.y, rect.y + rect.height);
}

export function inRange(value, min, max) {
    return value >= Math.min(min, max) && value <= Math.max(min, max);
}

export function rangeIntersect(min0, max0, min1, max1) {
    return Math.max(min0, max0) >= Math.min(min1, max1) && 
           Math.min(min0, max0) <= Math.max(min1, max1);
}

export function rectIntersect(r0, r1) {
    return rangeIntersect(r0.x, r0.x + r0.width, r1.x, r1.x + r1.width) &&
           rangeIntersect(r0.y, r0.y + r0.height, r1.y, r1.y + r1.height);
}

export function roundToPlaces(value, places) {
    var mult = Math.pow(10, places);
    return Math.round(value * mult) / mult;
}