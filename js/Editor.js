import { _c } from './utils/GeneralUtils';

export default class Editor {

    /**
     * Set initial properties
     * @param {string} projectName 
     */
    constructor(projectName){
        this.parent_el = $('.editor-canvas');
        this.ctx = null;
        this.width = this.parent_el.width();
        this.height = window.innerHeight;
        this.projectName = projectName;
        this.components = []; // stores all components added to the canvas window for rendering
    }

    /**
     * Reset editor properties back to default
     * @return void
     */
    reset(){
        this.ctx = null;
        this.components = [];
    }

    /**
     * Creates the Canvas Context
     * @param {Canvas} canvas 
     * @return void
     */
    createContext(canvas) {
        this.ctx = canvas[0].getContext('2d');
    }

    /**
     * Creates a new canvas and attaches it to the body
     * Also creates the 2D context
     * @return void
     */
    setCanvas(){

        var canvas = $('<canvas></canvas>')
            .attr('id', 'canvas')
            .attr('width', this.width * window.devicePixelRatio)
            .attr('height', this.height * window.devicePixelRatio)
            .css({
                width: '100%',
                height: '100%'
            });

        canvas.width = canvas.offsetWidth;
        canvas.height = this.height;

        $('body .editor-canvas').append(canvas);

        this.createContext(canvas);
    }

    /**
     * Clears everything drawn on the canvas
     * @return void
     */
    clearCanvas() {
        this.ctx.clearRect(0, 0, this.width, this.height);
    }

    /**
     * Adds a given component to the canvas
     * @param {object} component 
     */
    add(component){
        var last_id = this.components.length;
        this.components.push({ id: last_id, component: component });
    }

    /**
     * Updates a given component with specified id
     * @param {object} component 
     */
    update(component){
        this.components.map( item => {
            if(item.id === component.id) {
                item = component;
            }
        });
    }

    /**
     * Renders components on the editor canvas
     * @return void
     */
    render(){
        // clear everything
        this.clearCanvas();

        var componentLen = this.components.length;
        // console.log('Number of components: ' + componentLen);
        // if there are any components to render
        if(componentLen >= 1){
            // loop through components and render them
            this.components.forEach( (item) => {
                item.component.draw(this.ctx);
            });
        }
    }
}