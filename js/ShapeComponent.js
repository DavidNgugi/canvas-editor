
export default class ShapeComponent{

    constructor(x, y){
        this.name = "";
        this.x = x;
        this.y = y;
        this.borderWidth = 1;
        this.borderColor = 'black';
        this.fillColor = 'grey';
        this.opacity = 1;
    }

    get position(){
        return { x : this.x, y : this.y }
    }
}