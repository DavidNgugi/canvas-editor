import Rectangle from './Rectangle';

export default class ImageItem extends Rectangle {
    
    constructor(){
        super();
        this.borderWidth = 1;
        this.borderColor = 0;
        this.position = 0;
    }

    /**
     * Scales an ImageItem according to the given options
     * 
     * @param {Object} ctx 
     * @param {Object} options - { factor:int, xOffset: int, yOffset: int }
     * @return void
     */
    scale(ctx, options){
        //
    }
    
}