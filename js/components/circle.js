import ShapeComponent from '../ShapeComponent';
import * as Math from '../utils/MathUtil';

export default class Circle extends ShapeComponent {

    constructor(){
        super();
        this.radius = 5;
        this.rotAngle = 0; 
    }

    /**
     * Set the rotational angle value in radians
     */
    set rotAngle(value){
        this.rotAngle = Math.toRadians(value);
    }

    /**
     * Get Rotation angle in degrees
     * @return {number}
     */
    get rotAngle(){
        return Math.toDegrees(this.rotAngle);
    }

    /**
     * Get the centre position of the circle
     * @return {object}
     */
    get centrePosition(){
        return { x: this.x + this.radius, y: this.y + this.radius};
    }

    /**
     * Draw a circle given the options
     * @param {object} ctx 
     * @param {number} x 
     * @param {number} y 
     * @param {number} rotAngle 
     * @param {number} radius 
     * @return void
     */
    draw(ctx, x, y, rotAngle = null, radius = null){
        this.rotAngle = (rotAngle !== null) ? Math.toRadians(rotAngle) : 0;
        this.radius = (radius !== null) ? radius : 5;
        ctx.beginPath();
        ctx.arc(x, y, this.radius , this.rotAngle, 2 * Math.PI);
        ctx.stroke();
    }

}