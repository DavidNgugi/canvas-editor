import Rectangle from './Rectangle';
import { getTextHeight } from '../utils/GeneralUtils';

export default class TxtBox extends Rectangle {

    /**
     * Set initial properties
     * @param {object} options 
     */
    constructor(options){
        super(options.x, options.y);
        this.name = options.name || "";
        this.text = options.text;
        this.paddingAll = options.paddingAll || 10;
        this.padding = { top: 0, right: 0, bottom: 0, left: 0 };
        this.margin = options.margin || 0;
        this.fontType = "Calibri";
        this.fontSize = 16;
        this.font = this.fontSize + ' ' + this.fontType;
        this.textColor = "black";
        this.textAlignment = 'left';
        this.lineHeight = 25;
        this.borderColor = "lightblue";
        this.isDragging = false;
    }

    /**
     * Converts all properties and their values to JSON for saving purposes
     * 
     * @return {Object} - JSON
     */
    toJSON(){
        return JSON.stringify
        ({
            x               : this.x,
            y               : this.y,
            width           : this.width,
            height          : this.height,
            paddingAll      : this.paddingAll,
            padding         : this.padding,
            margin          : this.margin,
            borderWidth     : this.borderWidth,
            borderColor     : this.borderColor,
            position        : this.position,
            textColor       : this.textColor,
            text            : this.text,
            textAlignment   : this.textAlignment,
            lineHeight      : this.lineHeight,
            fontType        : this.fontType,
            fontSize        : this.fontSize,
            fillColor       : this.fillColor,
            isDragging      : this.isDragging
        });
    }

    /**
     * Scales a text box according to the given options
     * 
     * @param {Object} ctx 
     * @param {Object} options - { factor:int, xOffset: int, yOffset: int }
     * @return void
     */
    scale(ctx, options){
        //
    }

    /**
     * Resize the textbox
     * @param {object} ctx 
     * @param {int} xOffset 
     * @param {int} yOffset 
     */
    resize(ctx, xOffset, yOffset){
        //
    }

    /**
     * Draw the TextBox
     * @param {object} ctx 
     */
    draw(ctx){
        // draw rectangle and wrap the text around it
        super.draw(ctx, {
            x: this.x - 10,
            y: this.y - this.height/2, 
            width: this.width + this.paddingAll, 
            height: this.height + this.paddingAll,
            borderWidth: this.borderWidth,
            borderColor: this.borderColor
        });
        
        this.drawText(ctx);
    }

    /**
     * Draw text to canvas
     * @param {object} ctx 
     */
    drawText(ctx){
        var words = this.text.split(' ');

        var line = '';

        this.height = getTextHeight(this.font).height;

        for(var n = 0; n < words.length; n++) {
          var testLine = line + words[n] + ' ';
          var metrics = ctx.measureText(testLine);
          this.width = metrics.width;
          this.height = this.lineHeight;
          if (this.width > this.maxWidth && n > 0) {
            ctx.fillText(line, this.x, this.y);
            line = words[n] + ' ';
            this.y += this.lineHeight;
            this.height += this.lineHeight;
          }
          else {
            line = testLine;
            this.height += this.lineHeight;
          }
        }

        ctx.lineWidth = this.borderWidth;
        ctx.strokeStyle = this.borderColor;
        ctx.textAlign = this.textAlignment;
        ctx.textBaseline = 'top';
        ctx.font = this.font;
        ctx.fillStyle = this.textColor;
        ctx.fillText(line, this.x, this.y);
    }

    
}