import ShapeComponent from '../ShapeComponent';

export default class Rectangle extends ShapeComponent {

    constructor(x, y){
        super(x, y);
        this.minWidth = 50;
        this.maxWidth = 400;
        this.minHeight = 50;
        this.width = 0;
        this.height = 0;
    }

    /**
     * Returns an object representation of the dimensions of the rectangle
     * @return {object}
     */
    get Dimensions(){
        return { width: this.width, height: this.height };
    }

    /**
     * Get centre position
     * @return {object}
     */
    get centrePosition(){
        return { x: this.x + this.width / 2, y: this.y + this.height / 2};
    }

    /**
     * Scales a Rectangle component according to the given options
     * 
     * @param {Object} ctx 
     * @param {Object} options - { factor:int, xOffset: int, yOffset: int }
     * @return void
     */
    scale(ctx, options){
        //
    }

    draw(ctx, options){
        ctx.beginPath();
        ctx.lineWidth = options.borderWidth;
        ctx.strokeStyle = options.borderColor;      
        ctx.rect(options.x, options.y, options.width, options.height);
        ctx.stroke();
    }

}