// Ensure compatibility across browsers
var requestAnimationFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function(callback, element){
              window.setTimeout(callback, 1000 / 60);
            };
})();

import $ from 'jquery';
import Editor from './Editor';
import EventListener from './EventListener';
import TxtBox from './components/TxtBox';
import { resizeCanvasToDisplaySize } from './utils/GeneralUtils';

$(document).ready(function() {

    var editor = new Editor('new design'), listener;

    editor.add(new TxtBox({
        name: 'first',
        text: "Hello My friend, hello my lover. this is me again ...",
        x: 50, 
        y: 50,
        paddingAll: 20
    }));

    editor.add(new TxtBox({
        name: 'second',
        text: "Hello Guest",
        x: 400, 
        y: 250,
        paddingAll: 20
    }));

    var editorLoop = () => {

        resizeCanvasToDisplaySize(editor.ctx.canvas);

        editor.ctx.save();

        // Retina support
        // editor.ctx.scale(window.devicePixelRatio, window.devicePixelRatio);

        editor.clearCanvas();

        editor.render();

        editor.ctx.restore();

        requestAnimationFrame(editorLoop);
    };

    var main = () => {
        editor.setCanvas();

        // setupEventListeners()
        listener = new EventListener(editor);

        listener.listenToAllEvents();

        // editorLoop();

        // setup editor
        requestAnimationFrame(editorLoop);
    };

    main();


});